import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ServiceTwoTest {

    @Test
    void calculate() {
        ServiceTwo serviceTwo = new ServiceTwo();
        Assertions.assertEquals(3, serviceTwo.calculate(1, 2));
    }

    @Test
    void hello() {
        ServiceTwo serviceTwo = new ServiceTwo();
        Assertions.assertEquals("hello world", serviceTwo.hello("world"));
    }
}