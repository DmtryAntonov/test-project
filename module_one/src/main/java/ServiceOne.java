public class ServiceOne {
    
    public int calculate(int a, int b) {
        return a + b;
    }

    public String hello(String name) {
        return "hello " + name;
    }
}
