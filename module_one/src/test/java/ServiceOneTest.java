import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ServiceOneTest {

    @Test
    void calculate() {
        ServiceOne serviceOne = new ServiceOne();
        Assertions.assertEquals(3, serviceOne.calculate(1, 2));
    }

    @Test
    void hello() {
        ServiceOne serviceOne = new ServiceOne();
        Assertions.assertEquals("hello world", serviceOne.hello("world"));
    }
}